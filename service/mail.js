
const sgMail = require('@sendgrid/mail');
const config = require('config')

function send_mail(usuario){
    sgMail.setApiKey(process.env.SENDGRID_API_KEY || config.get("SENDGRID_API_KEY"));
    const msg = {
    to: usuario.email,
    from: 'thais@hub9.com.br',
    subject: 'Bootcamp TESTE',
    text: 'Olha meu email',
    html: `<h1>oi ${usuario.name}</h1>
    <br/>
    <p>Obrigada por se cadastrar no redev :)</p>`,
    };

    sgMail
    .send(msg)
    .then(() => {}, error => {
        console.error(error);

        if (error.response) {
        console.error(error.response.body)
        }
    });
}

module.exports = send_mail