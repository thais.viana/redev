const { app, server} = require('../server')
const request = require('supertest');
const mongoose = require('mongoose');
let res = null
let TOKEN = ""


beforeAll(async () =>{
  res = request(app)
  await res.post('/auth/')
    .send({
      email: "andre.luiz@al.infnet.com.br",
      password: "a12345678c"
    }).then(response => {
      TOKEN = response.body.token
  })
})

const data = {
    "current": true,
    "school": "UFRJ",
    "degree": "Bacharelado",
    "fieldofstudy": "Ciência da COmputação",
    "from": "2009-08-01T00:00:00.000Z",
    "to": "2015-03-01T00:00:00.000Z",
    "description": "ok"
}

describe('POST /education', function() {
    it('should add education to user logged', function(done) {
      res.post('/education').send(data)
        .set('Accept', 'application/json')
        .set('x-auth-token', TOKEN)
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('should delete a education from logged user', function(done) {
        res.delete('/education')
        .send({current : true})
          .set('Accept', 'application/json')
          .set('x-auth-token', TOKEN)
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
  });

afterAll(async ()=>{
    server.close()
    await mongoose.connection.close()
})