const express = require('express')
const router = express.Router();
const Topic = require('../../models/topic')
const MSGS = require('../../messages')

// @route    GET /topic
// @desc     LIST topics
// @access   Public
router.get('/', async (req, res, next) => {
    try {
        const topics = await Topic.find({})
        res.json(topics)
    } catch (err) {
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})


// @route    POST /topic
// @desc     create topic
// @access   Public
router.post('/', async (req, res, next) => {
    try {
        let topic = new Topic(req.body)
        await topic.save()
        if (topic.id) {
          res.json(topic);
        }
    } catch (err) {
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

module.exports = router;